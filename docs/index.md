---
layout: home

hero:
    name:    laylatichy/nano
    tagline: fast, simple and minimal php framework
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

        -   theme: alt
            text:  benchmark
            link:  https://web-frameworks-benchmark.netlify.app/result?asc=0&l=php&order_by=level512

features:
    -   title:   simple and minimal, always
        icon:
            light: /icons/rocket.svg
            dark:  /icons/rocket-dark.svg
        details: |
                 nano is a simple and minimal php framework for building fast and reliable apis

    -   title:   fairly complete, sometimes fast, kinda interesting
        icon:
            light: /icons/eyes.svg
            dark:  /icons/eyes-dark.svg
        details: |
                 ¯\_(ツ)_/¯

    -   title:     one of the fastest php frameworks
        icon:
            light: /icons/fast.svg
            dark:  /icons/fast-dark.svg
        details:   |
                   check out the benchmark results
        link:      https://web-frameworks-benchmark.netlify.app/result?asc=0&l=php&order_by=level512
        link_text: web-frameworks-benchmark
        rel:       external
---
