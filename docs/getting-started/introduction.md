---
layout: doc
---

# introduction

## what is laylatichy/nano?

`laylatichy/nano` is a lightweight framework for building apis in php

built on top of [workerman](https://www.workerman.net/) and [fast-route](https://github.com/nikic/FastRoute)

simple, fast and easy to use
