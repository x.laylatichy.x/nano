---
layout: doc
---

# installation

to simply install the nano framework, run the following command:

```sh
composer require laylatichy/nano
```

##### or use one of our starter kits

- [basic](../templates/basic)

more coming soon