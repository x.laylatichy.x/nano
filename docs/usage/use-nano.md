---
layout: doc
---


<script setup>
const args = {
    useNano: [],
    start: [],
    withOptions: [
        {
            type: 'array',
            name: 'options',
        },
    ],  
    getCache: [],
    withCache: [
        {
            type: 'CacheStatus',
            name: 'option',
        },
        {
            type: 'int',
            name: 'mins',
        },
    ],
    withoutCache: [],
    getHeaders: [],
    withHeader: [
        {
            type: 'string',
            name: 'header',
        },
        {
            type: 'string',
            name: 'value',
        },
    ],
    getModules: [],
    getModule: [
        {
            type: 'string',
            name: 'name',
        },
    ],
    withModule: [
        {
            type: 'NanoModule',
            name: 'module',
        },
    ],
    getRouter: [],
    processes: [
        {
            type: 'int',
            name: 'count',
        },
    ],
    destroy: [],
};
</script>

##### laylatichy\nano\Nano

`useNano()` is a helper function to get the nano instance

## <Types fn="useNano" r="Nano" :args="args.useNano" /> {#useNano}

```php
useNano();
```

## <Types fn="start" r="void" :args="args.start" /> {#start}

start the nano server

```php
useNano()->start();
```

## <Types fn="withOptions" r="Nano" :args="args.withOptions" /> {#withOptions}

pass context options to the nano server

https://www.php.net/manual/en/function.stream-context-create.php

```php
useNano()->withOptions([
    'ssl' => [
        'local_cert' => '/path/to/cert.pem',
        'local_pk' => '/path/to/private.key',
    ],
]);
```

## <Types fn="getCache" r="Cache" :args="args.getCache" /> {#getCache}

get the cache instance

```php
useNano()->getCache(); // same as useCache()
```

## <Types fn="withCache" r="Nano" :args="args.withCache" /> {#withCache}

set cache status and cache time

```php
useNano()->withCache(CacheStatus::ENABLED, 60);
```

## <Types fn="withoutCache" r="Nano" :args="args.withoutCache" /> {#withoutCache}

disable cache

```php
useNano()->withoutCache();
```

## <Types fn="getHeaders" r="Headers" :args="args.getHeaders" /> {#getHeaders}

get the headers instance

```php
useNano()->getHeaders(); // same as useHeaders()
```

## <Types fn="withHeader" r="Nano" :args="args.withHeader" /> {#withHeader}

set a global header

```php
useNano()->withHeader('Content-Type', 'application/json');
```

## <Types fn="getModules" r="<NanoModule::class, NanoModule>[]" :args="args.getModules" /> {#getModules}

get all modules

```php
useNano()->getModules();
```

## <Types fn="getModule" r="NanoModule" :args="args.getModule" /> {#getModule}

get a module by class name

```php
useNano()->getModule(NanoModule::class);

// or
useNanoModule(NanoModule::class);
```

## <Types fn="withModule" r="Nano" :args="args.withModule" /> {#withModule}

add a module

```php
useNano()->withModule(new NanoModule());
```

## <Types fn="getRouter" r="Router" :args="args.getRouter" /> {#getRouter}

get the router instance

```php
useNano()->getRouter(); // same as useRouter()
```

## <Types fn="processes" r="Nano" :args="args.processes" /> {#processes}

set the number of processes to run

```php
useNano()->processes(4);
```

## <Types fn="destroy" r="void" :args="args.destroy" /> {#destroy}

destroy the nano instance

```php
useNano()->destroy();
```




