---
layout: doc
---

<script setup>
const args = {
    useEvent: [
        {
            type: 'string',
            name: 'class',
        },
    ],
};
</script>

##### laylatichy\nano\events

events are a way to extend the functionality of nano

they are called at specific points in the execution of nano, and allow you to listen for events and run code when they
occur

## <Types fn="useEvent" r="NanoEvent" :args="args.useEvent" /> {#useNanoEvent}

```php
class NanoStartListener implements NanoEventListener {
    public function handle(NanoStartEvent $event): void {
        // do something
    }
}

useEvent(NanoStart::class)->attach(new NanoStartListener());
```