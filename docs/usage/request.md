---
layout: doc
---


<script setup>
const args = {
    post: [
        { 
            type: '?string',
            name: 'key',
        }
    ],
    get: [
        { 
            type: '?string',
            name: 'key',
        }
    ],
    file: [
        { 
            type: '?string',
            name: 'key',
        }
    ],
    header: [
        { 
            type: '?string',
            name: 'key',
        }
    ],
    cookie: [
        { 
            type: '?string',
            name: 'key',
        }
    ],
    path: [],
    method: [],
    raw: [],
    context: {
        set: [
            { 
                type: 'string',
                name: 'key',
            },
            { 
                type: 'mixed',
                name: 'value',
            }
        ],
        get: [
            { 
                type: 'string',
                name: 'key',
            }
        ],
        has: [
            { 
                type: 'string',
                name: 'key',
            }
        ],
    },
};
</script>

##### laylatichy\nano\core\request\Request

access to request data

## <Types fn="post" r="mixed" :args="args.post" /> {#post}

```php
useRouter()->post('/post', function (Request $request): Response {
    $name = $request->post('name'); // $_POST['name']
    $data = $request->post(); // $_POST
    ...
});
```

## <Types fn="get" r="mixed" :args="args.get" /> {#get}

```php
useRouter()->get('/get', function (Request $request): Response {
    $name = $request->get('name'); // $_GET['name']
    $data = $request->get(); // $_GET
    ...
});
```

## <Types fn="file" r="mixed" :args="args.file" /> {#file}

```php
useRouter()->post('/file', function (Request $request): Response {
    $name = $request->file('name'); // $_FILES['name']
    $data = $request->file(); // $_FILES
    ...
});
```

## <Types fn="header" r="mixed" :args="args.header" /> {#header}

```php
useRouter()->get('/header', function (Request $request): Response {
    $name = $request->header('name');
    $data = $request->header();
    ...
});
```

## <Types fn="cookie" r="mixed" :args="args.cookie" /> {#cookie}

```php
useRouter()->get('/cookie', function (Request $request): Response {
    $name = $request->cookie('name');
    $data = $request->cookie();
    ...
});
```

## <Types fn="path" r="mixed" :args="args.path" /> {#path}

```php
useRouter()->get('/path/{id}', function (Request $request) {
    $path = $request->path();
    ...
});
```

## <Types fn="method" r="string" :args="args.method" /> {#method}

```php
useRouter()->get('/path/{id}', function (Request $request) {
    $path = $request->method();
    ...
});
```

## <Types fn="raw" r="string" :args="args.raw" /> {#raw}

get raw request body data as string, useful for services that offer signature verification like stripe

```php
useRouter()->get('/raw', function (Request $request) {
    $raw = $request->raw();
    ...
});
```

## <Types fn="context->set" r="void" :args="args.context.set" /> {#context-set}

```php
useRouter()->get('/context/set', function (Request $request): Response {
    $request->context->set('key', 'value');
    ...
});
```

## <Types fn="context->get" r="mixed" :args="args.context.get" /> {#context-get}

```php
useRouter()->get('/context/get', function (Request $request): Response {
    $value = $request->context->get('key');
    ...
});
```

## <Types fn="context->has" r="bool" :args="args.context.has" /> {#context-has}

```php
useRouter()->get('/context/has', function (Request $request): Response {
    $has = $request->context->has('key');
    ...
});
```
