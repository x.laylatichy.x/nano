---
layout: doc
---

<script setup>
const args = {
    useResponse: [],
    getCode: [],
    withCode: [
        {
            type: 'HttpCode',
            name: 'code',
        },
    ],
    getHeaders: [],
    withHeader: [
        {
            type: 'string',
            name: 'key',
        },
        {
            type: 'string',
            name: 'value',
        },
    ],
    getBody: [],
    withText: [
        {
            type: 'string',
            name: 'body',
        },
    ],
    withJson: [
        {
            type: 'array',
            name: 'body',
        },
    ],
    useStatusResponse: [],
    useNotFoundResponse: [],
};
</script>

##### laylatichy\nano\core\response\Response

## <Types fn="useResponse" r="Response" :args="args.useResponse" /> {#useResponse}


```php
useRouter()->get('/', fn (): Response => useResponse());

// generate response with code 200, empty body and no headers
```

## <Types fn="getCode" r="HttpCode" :args="args.getCode" /> {#getCode}

```php
useResponse()->getCode(); // HttpCode::OK
```

## <Types fn="withCode" r="Response" :args="args.withCode" /> {#withCode}

set response code

```php
useRouter()->get('/', fn (): Response => useResponse()
    ->withCode(HttpCode::NOT_FOUND));
```

## <Types fn="getHeaders" r="array" :args="args.getHeaders" /> {#getHeaders}

```php
useResponse()->getHeaders(); // []
```

## <Types fn="withHeader" r="Response" :args="args.withHeader" /> {#withHeader}

set response header

```php
useRouter()->get('/', fn (): Response => useResponse()
    ->withHeader('Content-Type', 'application/json'));
```

## <Types fn="getBody" r="string" :args="args.getBody" /> {#getBody}

```php
useResponse()->getBody(); // ''
```

## <Types fn="withText" r="Response" :args="args.withText" /> {#withText}

set response body as text

```php
useRouter()->get('/', fn (): Response => useResponse()
    ->withText('Hello, World!'));
```

## <Types fn="withJson" r="Response" :args="args.withJson" /> {#withJson}

set response body as json

```php
useRouter()->get('/', fn (): Response => useResponse()
    ->withJson(['message' => 'Hello, World!']));
```

## <Types fn="useStatusResponse" r="Response" :args="args.useStatusResponse" /> {#useStatusResponse}

generate server status response

```php
useRouter()->get('/', fn (): Response => useStatusResponse());
```

## <Types fn="useNotFoundResponse" r="Response" :args="args.useNotFoundResponse" /> {#useNotFoundResponse}

generate not found response

```php
useRouter()->get('/', fn (): Response => useNotFoundResponse());
```
