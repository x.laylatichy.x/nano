---
layout: doc
---


<script setup>
const args = {
    useHeaders: [],
    get: [],
    getAll: [],
    add: [
        {
            type: 'string',
            name: 'header',
        },
        {
            type: 'string',
            name: 'value',
        },
    ],
};
</script>

##### laylatichy\nano\core\headers\Headers

`useHeaders()` is a helper function to get the headers instance

## <Types fn="useHeaders" r="Headers" :args="args.useHeaders" /> {#useHeaders}

```php
useHeaders();
```

## <Types fn="get" r="?string" :args="args.get" /> {#get}

```php
useHeaders()->get('Content-Type');
```

## <Types fn="getAll" r="<string, string>[]" :args="args.getAll" /> {#getAll}

```php
useHeaders()->getAll();
```

## <Types fn="add" r="Headers" :args="args.add" /> {#add}

```php
useHeaders()->add('Content-Type', 'application/json');
```

