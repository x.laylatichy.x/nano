---
layout: doc
---

<script setup>
const args = {
    defineMiddleware: [
        {
            type: 'string',
            name: 'name',
        },
        {
            type: 'callable',
            name: 'callback',
        },
    ],
};
</script>

## <Types fn="defineMiddleware" r="void" :args="args.defineMiddleware" /> {#defineMiddleware}

```php
defineMiddleware('my-middleware', function (): void => {
    // do something
});
```

use defined middleware

```php
useRouter()->get('/', fn (): Response => useResponse()
    ->withJson([
        'with middleware' => 'with middleware',
    ]), ['my-middleware']);
```