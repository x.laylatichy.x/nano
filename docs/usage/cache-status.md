---
layout: doc
---


<script setup>
const args = {
    getStatus: [],
};
</script>

##### laylatichy\nano\core\cache\CacheStatus

CacheStatus is a simple enum class to define the cache status

```php
CacheStatus::ENABLED;
CacheStatus::DISABLED;
```

## <Types fn="getStatus" r="bool" :args="args.getStatus" /> {#getStatus}

```php
CacheStatus::ENABLED->getStatus(); // true
CacheStatus::DISABLED->getStatus(); // false
```

