---
layout: doc
---


<script setup>
const args = {
    useCache: [],
    getDuration: [],
    withDuration: [
        {
            type: 'int',
            name: 'minutes',
        },
    ],
    getStatus: [],
    withStatus: [
        {
            type: 'CacheStatus',
            name: 'status',
        },
    ],
    disable: [],
    enable: [],
    isEnabled: [],
};
</script>

##### laylatichy\nano\core\cache\Cache

`useCache()` is a helper function to get the cache instance

## <Types fn="useCache" r="Cache" :args="args.useCache" /> {#useCache}

```php
useCache();
```

## <Types fn="getDuration" r="int" :args="args.getDuration" /> {#getDuration}

```php
useCache()->getDuration();
```

## <Types fn="withDuration" r="Cache" :args="args.withDuration" /> {#withDuration}

```php
useCache()->withDuration(10);
```

## <Types fn="getStatus" r="CacheStatus" :args="args.getStatus" /> {#getStatus}

```php
useCache()->getStatus();
```

## <Types fn="withStatus" r="Cache" :args="args.withStatus" /> {#withStatus}

```php
useCache()->withStatus(CacheStatus::ENABLED);
```

## <Types fn="disable" r="Cache" :args="args.disable" /> {#disable}

```php
useCache()->disable();
```

## <Types fn="enable" r="Cache" :args="args.enable" /> {#enable}

```php
useCache()->enable();
```

## <Types fn="isEnabled" r="bool" :args="args.isEnabled" /> {#isEnabled}

```php
useCache()->isEnabled();
```


