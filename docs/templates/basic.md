---
layout: doc
---

# examples/basic

basic example of a nano application

## installation

```sh
npx tiged -s gitlab:nano8/templates/basic nano-basic

# if you run into issues with the above command, try adding the --mode=git flag
npx tiged -s --mode=git gitlab:nano8/templates/basic nano-basic
```

## enter example directory

```sh
cd nano-basic
```

## install npm dependencies

```sh
npm ci
```

## install composer dependencies

```sh
composer install
```

## run the development server

```sh
npm run dev

# or in the watch mode
npm run dev:watch # will restart the server on file changes in the src directory
```

## visit the example in your browser

open http://localhost:3000

or

open http://localhost:3000/hello

