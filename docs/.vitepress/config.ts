import { defineConfig } from 'vitepress';

export default defineConfig({
    lastUpdated:     true,
    ignoreDeadLinks: true,
    sitemap:         {
        hostname: 'https://nano.laylatichy.com/',
    },
    title:           'laylatichy/nano',
    titleTemplate:   'laylatichy/nano - documentation',
    description:     'laylatichy/nano | minimal php framework',
    themeConfig:     {
        lastUpdated: {
            text:          'updated',
            formatOptions: {
                dateStyle: 'long',
                timeStyle: 'medium',
            },
        },
        search:      {
            provider: 'local',
        },
        nav:         [],
        sidebar:     [
            {
                text:  'getting started',
                items: [
                    {
                        text: 'introduction',
                        link: '/getting-started/introduction',
                    },
                    {
                        text: 'installation',
                        link: '/getting-started/installation',
                    },
                ],
            },
            {
                text:  'usage',
                items: [
                    {
                        text: 'useNano',
                        link: '/usage/use-nano',
                    },
                    {
                        text: 'useCache',
                        link: '/usage/use-cache',
                    },
                    {
                        text: 'CacheStatus',
                        link: '/usage/cache-status',
                    },
                    {
                        text: 'useHeaders',
                        link: '/usage/use-headers',
                    },
                    {
                        text: 'Request',
                        link: '/usage/request',
                    },
                    {
                        text: 'useResponse',
                        link: '/usage/use-response',
                    },
                    {
                        text: 'defineMiddleware',
                        link: '/usage/define-middleware',
                    },
                    {
                        text: 'useEvent',
                        link: '/usage/use-event',
                    },
                ],
            },
            {
                text:  'core',
                items: [
                    {
                        text: 'config',
                        link: 'https://nano8.gitlab.io/core/config',
                    },
                    {
                        text: 'httpcode',
                        link: 'https://nano8.gitlab.io/core/httpcode',
                    },
                    {
                        text: 'exception',
                        link: 'https://nano8.gitlab.io/core/exception',
                    },
                ],
            },
            {
                text:  'modules',
                items: [
                    {
                        text: 'testing',
                        link: 'https://nano8.gitlab.io/modules/testing',
                    },
                    {
                        text: 'validator',
                        link: 'https://nano8.gitlab.io/modules/validator',
                    },
                    {
                        text: 'security',
                        link: 'https://nano8.gitlab.io/modules/security',
                    },
                    {
                        text: 'auth',
                        link: 'https://nano8.gitlab.io/modules/auth',
                    },
                    {
                        text: 'stripe',
                        link: 'https://nano8.gitlab.io/modules/stripe',
                    },
                    {
                        text: 'aws',
                        link: 'https://nano8.gitlab.io/modules/aws',
                    },
                    {
                        text: 'orm',
                        link: 'https://nano8.gitlab.io/modules/orm',
                    },
                    {
                        text: 'log',
                        link: 'https://nano8.gitlab.io/modules/log',
                    },
                    {
                        text: 'email',
                        link: 'https://nano8.gitlab.io/modules/email',
                    },
                    {
                        text: 'openapi',
                        link: 'https://nano8.gitlab.io/modules/openapi',
                    },
                ],
            },
            {
                text:  'starter kits',
                items: [
                    {
                        text: 'basic',
                        link: '/templates/basic',
                    },
                ],
            },
        ],
        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/x.laylatichy.x/nano',
            },
        ],
        editLink:    {
            text:    'edit this page',
            pattern: 'https://gitlab.com/x.laylatichy.x/nano/-/edit/dev/docs/:path',
        },
    },
});
