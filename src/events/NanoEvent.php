<?php

namespace laylatichy\nano\events;

abstract class NanoEvent {
    /**
     * @var NanoEventListener[]
     */
    public array $listeners = [];

    public function attach(NanoEventListener $listener): void {
        $this->listeners[] = $listener;
    }

    public function dispatch(NanoEventMessage $event): void {
        foreach ($this->listeners as $listener) {
            $listener->handle($event); // @phpstan-ignore-line
        }
    }
}