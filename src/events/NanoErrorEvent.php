<?php

namespace laylatichy\nano\events;

use Exception;
use Throwable;
use TypeError;
use ValueError;

class NanoErrorEvent implements NanoEventMessage {
    public function __construct(
        public Exception|Throwable|TypeError|ValueError $exception,
    ) {}
}