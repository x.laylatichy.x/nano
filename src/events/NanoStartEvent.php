<?php

namespace laylatichy\nano\events;

class NanoStartEvent implements NanoEventMessage {
    public function __construct(
        public string $message,
    ) {}
}