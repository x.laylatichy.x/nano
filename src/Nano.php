<?php

namespace laylatichy\nano;

use laylatichy\nano\core\cache\Cache;
use laylatichy\nano\core\cache\CacheStatus;
use laylatichy\nano\core\exception\NanoException;
use laylatichy\nano\core\exceptions\IException;
use laylatichy\nano\core\headers\Headers;
use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\core\request\Request;
use laylatichy\nano\core\response\Response;
use laylatichy\nano\core\router\Router;
use laylatichy\nano\core\router\Types;
use laylatichy\nano\events\NanoError;
use laylatichy\nano\events\NanoErrorEvent;
use laylatichy\nano\events\NanoEvent;
use laylatichy\nano\events\NanoStart;
use laylatichy\nano\events\NanoStartEvent;
use laylatichy\nano\modules\NanoModule;
use Throwable;
use TypeError;
use ValueError;
use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http\Request as WorkermanRequest;
use Workerman\Worker;

class Nano extends Worker {
    public string $name = __CLASS__;

    protected string $socket;

    protected array $options = [];

    /**
     * @var array<string, callable>
     */
    private array $middlewares = [];

    private static ?Nano $_instance   = null;

    private array $modules     = [];

    private array $events      = [];

    public function __construct(
        public Router $router = new Router(),
        protected Headers $headers = new Headers(),
        protected Cache $cache = new Cache(CacheStatus::ENABLED, 60),
    ) {
        useConfig()->load();

        $this->clean();

        $this->processes();

        $this->withEvent(new NanoStart());
        $this->withEvent(new NanoError());
    }

    public static function getInstance(): self {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function start(): void {
        $this->socket = $this->getSocket();

        parent::__construct($this->socket, $this->options);

        $this->onMessage = [$this, 'onMessage'];

        // $this->initModules(); init modules when they are added

        $this->router->collectRoutes()->run();

        $this->getEvent(NanoStart::class)->dispatch(new NanoStartEvent('nano:start'));

        parent::runAll();
    }

    /**
     * @see https://www.php.net/manual/en/function.stream-context-create.php
     */
    public function withOptions(array $options): self {
        $this->options = $options;

        return $this;
    }

    public function getCache(): Cache {
        return $this->cache;
    }

    public function withCache(CacheStatus $option = CacheStatus::ENABLED, int $mins = 60): self {
        $this->cache->withStatus($option)->withDuration($mins);

        return $this;
    }

    public function withoutCache(): self {
        $this->cache->disable();

        return $this;
    }

    public function getHeaders(): Headers {
        return $this->headers;
    }

    public function withHeader(string $header, string $value): self {
        $this->headers->add($header, $value);

        return $this;
    }

    /**
     * @return array<string, NanoModule>
     */
    public function getModules(): array {
        return $this->modules; // @phpstan-ignore-line
    }

    /**
     * @template T of NanoModule
     *
     * @param class-string<T> $class
     *
     * @return T
     */
    public function getModule(string $class): NanoModule {
        if (!isset($this->modules[$class])) {
            useNanoException("module {$class} not found");
        }

        return $this->modules[$class]; // @phpstan-ignore-line
    }

    /**
     * @template T of NanoModule
     *
     * @param T $module
     */
    public function withModule(NanoModule $module): self {
        if (isset($this->modules[$module::class])) {
            $name = $module::class;

            useNanoException("module {$name} already installed");
        }

        $this->modules[$module::class] = $module;

        $module->register($this);

        return $this;
    }

    /**
     * @template T of NanoEvent
     *
     * @param T $event
     */
    public function withEvent(NanoEvent $event): self {
        if (isset($this->events[$event::class])) {
            $name = $event::class;

            useNanoException("event {$name} already installed");
        }

        $this->events[$event::class] = $event;

        return $this;
    }

    /**
     * @template T of NanoEvent
     *
     * @param class-string<T> $class
     *
     * @return T
     */
    public function getEvent(string $class): NanoEvent {
        if (!isset($this->events[$class])) {
            useNanoException("event {$class} not found");
        }

        return $this->events[$class]; // @phpstan-ignore-line
    }

    public function withMiddleware(string $name, callable $callback): void {
        if (isset($this->middlewares[$name])) {
            useNanoException("middleware {$name} already exists");
        }

        $this->middlewares[$name] = $callback;
    }

    public function onMessage(TcpConnection $connection, WorkermanRequest $request): Response {
        if ($request->method() === Types::OPTIONS->name) {
            $res = useResponse()
                ->withCode(HttpCode::OK);

            $connection->send($res->generate($this->headers));

            return $res;
        }

        /** @var array<string, array{timestamp: int, response: Response}> $callbacks */
        static $callbacks = [];

        try {
            $callback = $callbacks[$request->method() . $request->path()] ?? null;

            if ($callback && $this->cache->isEnabled()) {
                if (time() < $callback['timestamp'] + ($this->cache->getDuration() * 60)) {
                    /** @var Response $res */
                    $res = $callback['response'];

                    $connection->send($res->generate($this->headers));

                    return $res;
                }

                unset($callbacks[$request->method() . $request->path()]);
            }

            $response = $this->router->dispatch($request->method(), $request->path());

            if ($response) {
                $callback = $response[1]->callback; // @phpstan-ignore-line
                $path     = $request->path();
                $args     = [];

                $req = new Request($request);

                if (is_array($response[2]) && count($response[2]) > 0) {
                    foreach ($response[2] as $k => $v) {
                        if (is_string($v)) {
                            $path = str_replace($v, "{{$k}}", $path);
                        }
                    }

                    $args     = array_values($response[2]);
                    $callback = function ($request) use ($req, $args, $callback) {
                        return $callback($req, ...$args); // @phpstan-ignore-line
                    };
                }

                // @phpstan-ignore-next-line
                foreach ($response[1]->middlewares as $middleware) {
                    if (!isset($this->middlewares[$middleware])) {
                        useNanoException("middleware {$middleware} not found"); // @phpstan-ignore-line
                    }

                    $this->middlewares[$middleware]($req, ...$args);
                }

                /** @var Response $res */
                $res = $callback($req); // @phpstan-ignore-line

                if ($this->cache->isEnabled()) {
                    $callbacks[$request->method() . $request->path()] = [
                        'response'  => $res,
                        'timestamp' => time(),
                    ];
                }

                $connection->send($res->generate($this->headers));

                return $res;
            }

            $res = useNotFoundResponse();

            $connection->send($res->generate($this->headers));

            return $res;
        } catch (TypeError|ValueError $e) {
            $res = useResponse()
                ->withCode(HttpCode::BAD_REQUEST)
                ->withJson([
                    'code'     => HttpCode::BAD_REQUEST->code(),
                    'response' => $e->getMessage(),
                ]);

            $connection->send($res->generate($this->headers));

            $this->getEvent(NanoError::class)->dispatch(new NanoErrorEvent($e));

            return $res;
        } catch (IException $e) {
            $res = $e->response();

            $connection->send($res->generate($this->headers));

            return $res;
        } catch (NanoException $e) {
            $res = useResponse()
                ->withCode(HttpCode::from($e->getCode()))
                ->withJson([
                    'code'     => $e->getCode(),
                    'response' => $e->getMessage(),
                ]);

            $connection->send($res->generate($this->headers));

            if ($e->getCode() === HttpCode::INTERNAL_SERVER_ERROR->code()) {
                $this->getEvent(NanoError::class)->dispatch(new NanoErrorEvent($e));
            }

            return $res;
        } catch (Throwable $e) {
            $res = useResponse()
                ->withCode(HttpCode::INTERNAL_SERVER_ERROR)
                ->withJson([
                    'code'     => HttpCode::INTERNAL_SERVER_ERROR->code(),
                    'response' => $e->getMessage(),
                ]);

            $connection->send($res->generate($this->headers));

            $this->getEvent(NanoError::class)->dispatch(new NanoErrorEvent($e));

            return $res;
        }
    }

    public function getRouter(): Router {
        return $this->router;
    }

    public function status(): Response {
        return useResponse()
            ->withCode(HttpCode::OK)
            ->withJson([
                'code'         => HttpCode::OK->code(),
                'hostname'     => $_SERVER['HOSTNAME'] ?? $this->getSocket(),
                'requestTime'  => time(),
                'workersCount' => count(static::getAllWorkers()),
            ]);
    }

    public function notFound(): Response {
        return useResponse()
            ->withCode(HttpCode::NOT_FOUND)
            ->withJson([
                'code'     => HttpCode::NOT_FOUND->code(),
                'response' => 'not found',
            ]);
    }

    public function processes(?int $count = null): self {
        $nproc       = (int)shell_exec('nproc') * 2;
        $this->count = $count ?? ($nproc ?? 8); // @phpstan-ignore-line

        return $this;
    }

    public function destroy(): void {
        $this->stop();

        self::$_instance = null;
    }

    //    private function initModules(): void {
    //        foreach ($this->modules as $class => $module) {
    //            $module->register($this);
    //        }
    //    }

    private function clean(): void {
        $files = glob(useConfig()->getRoot() . '/vendor/workerman/*.pid');

        is_array($files) && array_map('unlink', $files);
    }

    private function getSocket(): string {
        return useConfig()->getHost() . ':' . useConfig()->getPort();
    }
}
