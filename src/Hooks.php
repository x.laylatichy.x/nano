<?php

use laylatichy\nano\core\cache\Cache;
use laylatichy\nano\core\exceptions\BadRequestException;
use laylatichy\nano\core\headers\Headers;
use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\core\response\Response;
use laylatichy\nano\core\router\Router;
use laylatichy\nano\events\NanoEvent;
use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\Nano;

if (!function_exists('useNano')) {
    function useNano(): Nano {
        return Nano::getInstance();
    }
}

if (!function_exists('useNanoModule')) {
    /**
     * @template T of NanoModule
     *
     * @param class-string<T> $class
     *
     * @return T
     */
    function useNanoModule(string $class): NanoModule {
        return useNano()->getModule($class);
    }
}

if (!function_exists('useEvent')) {
    /**
     * @template T of NanoEvent
     *
     * @param class-string<T> $class
     *
     * @return T
     */
    function useEvent(string $class): NanoEvent {
        return useNano()->getEvent($class);
    }
}

if (!function_exists('useCache')) {
    function useCache(): Cache {
        return useNano()->getCache();
    }
}

if (!function_exists('useHeaders')) {
    function useHeaders(): Headers {
        return useNano()->getHeaders();
    }
}

if (!function_exists('useRouter')) {
    function useRouter(): Router {
        return useNano()->getRouter();
    }
}

if (!function_exists('useResponse')) {
    function useResponse(): Response {
        return new Response();
    }
}

if (!function_exists('useStatusResponse')) {
    function useStatusResponse(): Response {
        return useNano()->status();
    }
}

if (!function_exists('useNotFoundResponse')) {
    function useNotFoundResponse(): Response {
        return useNano()->notFound();
    }
}

if (!function_exists('useStandardResponse')) {
    function useStandardResponse(mixed $data): Response {
        return useResponse()->withJson([
            'response' => $data,
        ]);
    }
}

if (!function_exists('useCreatedResponse')) {
    function useCreatedResponse(mixed $data): Response {
        return useResponse()
            ->withCode(HttpCode::CREATED)
            ->withJson([
                'response' => $data,
            ]);
    }
}

if (!function_exists('defineMiddleware')) {
    function defineMiddleware(string $name, callable $callback): void {
        useNano()->withMiddleware($name, $callback);
    }
}

// exceptions
if (!function_exists('useBadRequestException')) {
    /**
     * @param string[] $data
     *
     * @throws BadRequestException
     */
    function useBadRequestException(array $data = []): void {
        throw new BadRequestException($data);
    }
}