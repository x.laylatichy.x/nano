<?php

namespace laylatichy\nano\modules;

use laylatichy\nano\Nano;

interface NanoModule {
    public function register(Nano $nano): void;
}