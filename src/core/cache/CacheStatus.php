<?php

namespace laylatichy\nano\core\cache;

enum CacheStatus {
    case ENABLED;
    case DISABLED;

    public function getStatus(): bool {
        return match ($this) {
            self::ENABLED  => true,
            self::DISABLED => false,
        };
    }
}
