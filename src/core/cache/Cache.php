<?php

namespace laylatichy\nano\core\cache;

class Cache {
    public function __construct(
        private CacheStatus $status = CacheStatus::ENABLED,
        private int $duration = 60,
    ) {}

    public function getDuration(): int {
        return $this->duration;
    }

    public function withDuration(int $minutes): self {
        $this->duration = $minutes;

        return $this;
    }

    public function getStatus(): CacheStatus {
        return $this->status;
    }

    public function withStatus(CacheStatus $status): self {
        $this->status = $status;

        return $this;
    }

    public function disable(): self {
        $this->status = CacheStatus::DISABLED;

        return $this;
    }

    public function enable(): self {
        $this->status = CacheStatus::ENABLED;

        return $this;
    }

    public function isEnabled(): bool {
        return $this->status->getStatus();
    }
}
