<?php

namespace laylatichy\nano\core\headers;

class Headers {
    /**
     * @param array<string, string> $headers
     */
    public function __construct(
        private array $headers = [],
    ) {
        $this->add('X-Server', 'laylatichy/nano');
        $this->add('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
        $this->add('Access-Control-Allow-Headers', 'Content-Type');
    }

    public function get(string $header): ?string {
        return $this->headers[$header] ?? null;
    }

    public function getAll(): array {
        return $this->headers;
    }

    public function add(string $header, string $value): self {
        $this->headers[$header] = $value;

        return $this;
    }
}
