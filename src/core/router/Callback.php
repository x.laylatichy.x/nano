<?php

namespace laylatichy\nano\core\router;

use Closure;

class Callback {
    /**
     * @param list<string> $middlewares
     */
    public function __construct(
        public string $path,
        public Closure &$callback,
        public array $middlewares = [],
    ) {
        // set path and callback
    }
}
