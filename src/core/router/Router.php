<?php

namespace laylatichy\nano\core\router;

use Closure;
use Exception;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Symfony\Component\Finder\Finder;

use function FastRoute\simpleDispatcher;

class Router {
    private ?Dispatcher $dispatcher = null;

    /**
     * @var array<string, array<string, Callback>>
     */
    private array $routes = [];

    public function __construct() {}

    /** @param list<string> $middlewares */
    public function get(string $path, Closure $callback, array $middlewares = []): void {
        $this->addRoute(Types::GET, $path, $callback, $middlewares);
    }

    /** @param list<string> $middlewares */
    public function post(string $path, Closure $callback, array $middlewares = []): void {
        $this->addRoute(Types::POST, $path, $callback, $middlewares);
    }

    /** @param list<string> $middlewares */
    public function put(string $path, Closure $callback, array $middlewares = []): void {
        $this->addRoute(Types::PUT, $path, $callback, $middlewares);
    }

    /** @param list<string> $middlewares */
    public function patch(string $path, Closure $callback, array $middlewares = []): void {
        $this->addRoute(Types::PATCH, $path, $callback, $middlewares);
    }

    /** @param list<string> $middlewares */
    public function delete(string $path, Closure $callback, array $middlewares = []): void {
        $this->addRoute(Types::DELETE, $path, $callback, $middlewares);
    }

    public function dispatch(string $method, string $path): ?array {
        $dispatch = $this->dispatcher?->dispatch($method, $path);

        if (isset($dispatch[0]) && is_numeric($dispatch[0]) && (int)$dispatch[0] === Dispatcher::FOUND) {
            return $dispatch;
        }

        return null;
    }

    public function run(): void {
        $this->dispatcher = simpleDispatcher(
            function (RouteCollector $route) {
                foreach ($this->getRoutes() as $method => $routes) {
                    // @phpstan-ignore-next-line
                    foreach ($routes as $path => $callback) {
                        // @phpstan-ignore-next-line
                        $route->addRoute($method, $callback->path, $callback);
                    }
                }
            }
        );
    }

    public function collectRoutes(): self {
        $finder = new Finder();

        try {
            $finder
                ->files()
                ->in(useConfig()->getRoot())
                ->name('*.php')
                ->contains('useRouter()');

            foreach ($finder as $file) {
                if (file_exists($file->getRealPath())
                    && !in_array($file->getRealPath(), get_included_files(), true)) {
                    require_once $file->getRealPath();
                }
            }
        } catch (Exception $e) {
            // src directory not found
        }

        return $this;
    }

    public function getRoutes(): array {
        return $this->routes;
    }

    /**
     * @param list<string> $middlewares
     */
    private function addRoute(Types $method, string $path, Closure $callback, array $middlewares = []): void {
        if (isset($this->routes[$method->name][$path])) {
            useNanoException('route already exists');
        }

        $this->routes[$method->name][$path] = new Callback($path, $callback, $middlewares);
    }
}
