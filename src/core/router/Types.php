<?php

namespace laylatichy\nano\core\router;

enum Types: string {
    case GET     = 'get';
    case POST    = 'post';
    case PUT     = 'put';
    case PATCH   = 'patch';
    case DELETE  = 'delete';
    case OPTIONS = 'options';
    case HEAD    = 'head';
}
