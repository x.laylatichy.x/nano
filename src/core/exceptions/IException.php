<?php

namespace laylatichy\nano\core\exceptions;

use laylatichy\nano\core\response\Response;

interface IException {
    public function response(): Response;
}