<?php

namespace laylatichy\nano\core\exceptions;

use Exception;
use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\core\response\Response;

class BadRequestException extends Exception implements IException {
    private HttpCode $httpCode = HttpCode::BAD_REQUEST;

    /**
     * @param string[] $errors
     */
    public function __construct(private readonly array $errors = []) {
        parent::__construct('bad request exception', $this->httpCode->code());
    }

    public function response(): Response {
        return useResponse()
            ->withCode($this->httpCode)
            ->withJson([
                'code'     => $this->httpCode->code(),
                'response' => $this->errors,
            ]);
    }
}
