<?php

namespace laylatichy\nano\core\response;

use JsonException;
use laylatichy\nano\core\headers\Headers;
use laylatichy\nano\core\httpcode\HttpCode;
use Workerman\Protocols\Http\Response as WorkermanResponse;

class Response {
    /**
     * @param array<string, string> $headers
     */
    public function __construct(
        private HttpCode $code = HttpCode::OK,
        private string $body = '',
        private array $headers = [],
    ) {}

    public function getCode(): HttpCode {
        return $this->code;
    }

    public function withCode(HttpCode $code): self {
        $this->code = $code;

        return $this;
    }

    public function getHeaders(): array {
        return $this->headers;
    }

    public function withHeader(string $key, string $value): self {
        $this->headers[$key] = $value;

        return $this;
    }

    public function getBody(): string {
        return $this->body;
    }

    public function withText(string $body): self {
        $this->body = $body;

        return $this;
    }

    public function withJson(array $body): self {
        try {
            $this->body = json_encode($body, JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);
        } catch (JsonException) {
            useNanoException('invalid json', HttpCode::I_AM_A_TEAPOT);
        }

        return $this;
    }

    public function generate(Headers $headers): WorkermanResponse {
        return new WorkermanResponse(
            $this->code->code(),
            array_merge($headers->getAll(), $this->headers),
            $this->body
        );
    }
}

