<?php

namespace laylatichy\nano\core\request;

class RequestContext {
    /**
     * @var array<string, mixed>
     */
    private array $data = [];

    public function set(string $key, mixed $value): void {
        $this->data[$key] = $value;
    }

    public function get(string $key): mixed {
        if (!$this->has($key)) {
            useNanoException("key {$key} not found in request context");
        }

        return $this->data[$key];
    }

    public function has(string $key): bool {
        return isset($this->data[$key]);
    }
}

