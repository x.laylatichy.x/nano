<?php

namespace laylatichy\nano\core\request;

use Workerman\Protocols\Http\Request as WorkermanRequest;

class Request {
    public function __construct(
        private readonly WorkermanRequest $request,
        public readonly RequestContext $context = new RequestContext(),
    ) {}

    public function post(?string $key = null): mixed {
        return $key ? $this->request->post($key) : $this->request->post();
    }

    public function get(?string $key = null): mixed {
        return $key ? $this->request->get($key) : $this->request->get();
    }

    public function file(?string $key = null): mixed {
        return $key ? $this->request->file($key) : $this->request->file();
    }

    public function header(?string $key = null): mixed {
        return $key ? $this->request->header($key) : $this->request->header();
    }

    public function cookie(?string $key = null): mixed {
        return $key ? $this->request->cookie($key) : $this->request->cookie();
    }

    public function path(): mixed {
        return $this->request->path();
    }

    public function method(): string {
        return $this->request->method();
    }

    /**
     * get request raw body, useful payload sig verification by stripe etc.
     */
    public function raw(): string {
        return $this->request->rawBody();
    }
}

